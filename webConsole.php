<?php
/** Web console for php devs
 * @author Aidas R. <ramoaidas@gmail.com>
 * Date-Time: 2015-05-13 09:49
 *
 * @version 0.1.0
 */
$gitPullHost = '';
if (isset($_POST['command'])) {
    switch ($_POST['command']) {
        case 'git pull':
            $output = shell_exec('git pull '.$gitPullHost);
            break;
        case 'help':
            $output = 'Command list:
    help     - command list;
    clear    - clear screen;
    git pull - update project version;';
            break;
        default:
            $output = "This command doesn't exist. Use <b>help</b> command for more info.";
            break;
    }
    echo $output;
    exit;
}
?>
<!DOCTYPE html>
<html>
<head>
 <meta http-equiv="Pragma" content="no-cache">
    <meta http-equiv="Cache-Control" content="no-cache">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <style>
        body {
            background-color: #00695C;
        }

        h2 {
            color: #B2DFDB;
            text-align: center;
        }

        p {
            font-weight: 400;
            font-size: 20px;
        }

        #console-wrap {
            margin: auto;
            height: 450px;
            width: 806px;
            background-color: #26A69A;
        }

        #console-screen {
            margin: auto;
            height: 390px;
            width: 790px;
            position: relative;
            top: 8px;
            background-color: white;
            overflow: auto;
            padding-left: 5px;
        }

        #command-line {
            margin-top: 16px;
            margin-left: 8px;
            height: 30px;
            width: 630px;
            font-size: large;
            padding-left: 5px;
        }

        #execute {
            margin-left: 4px;
            height: 36px;
            width: 148px;
            background-color: #004D40;
            color: #1DE9B6;
            font-weight: bold;
            font-size: large;
        }

        #execute:hover {
            background-color: #00897B;
        }

    </style>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <script>
        $(document).ready(function(){
            var tagsToReplace = {
                '&': '&amp;',
                '<': '&lt;',
                '>': '&gt;'
            };

            function replaceTag(tag) {
                return tagsToReplace[tag] || tag;
            }

            function safe_tags_replace(str) {
                return str.replace(/[&<>]/g, replaceTag);
            }

            $("#send-message-area").submit(function(event) {
                var commandInput = $('#command-line').val();
                if (commandInput != 'clear') {
                    $.post(
                        'webConsole.php',{'command':commandInput}, function(data){

                            $("#console-screen").append('<pre>' + safe_tags_replace(data) + '</pre>');
                        }
                    );
                } else {
                    $("#console-screen").contents().remove();
                }
                $('#command-line').val('');
                event.preventDefault();
            });
        });
    </script>
</head>
<body style="height:100%;">
<div id="page-wrap">
    <h2>PHP console for php developers</h2>
    <div id="console-wrap"><div id="console-screen">
           <code> Use <b>help</b> command for more info</code>
        </div>
        <form id="send-message-area">
            <input type="text" id="command-line" maxlength = '100'/>
            <input type="submit" id="execute" value="Execute">
        </form>
    </div>
</div>
</body>
</html>